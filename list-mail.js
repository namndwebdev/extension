const URL_API = 'http://localhost:8080';
let user;

chrome.storage.local.get(['key'], function (result) {
  if (result.key && result.key.loggedIn) {
    user = result.key;

    //load infinite
    var $container = $('.container').infiniteScroll({
      path: function () {
        let url = URL_API + '/pmd-new/ListMailApi?token=' + user.token + '&type=extension';
        url += '&rwInfo={user_id: ' + user.userId + ', pageNumber: ' + this.pageIndex + '}'
        return url;
      },
      // load response as flat text
      responseType: 'text',
      status: '.scroll-status',
      history: false,
    });

    $container.on('load.infiniteScroll', function (event, response) {
      // parse response into JSON data
      var data = JSON.parse(response);
      // compile data into HTML
      var itemsHTML = data.map(getItemHTML);

      itemsHTML.forEach(function (item, index) {
        let $item = $(item);

        //add event keep and exclude
        $item.find('.exclude').click(exclude.bind(this, $item, user.token));
        $item.find('.keep').click(keep.bind(this, $item, user.token));

        $container.infiniteScroll('appendItems', $item);
      });
    });

    // load initial page
    $container.infiniteScroll('loadNextPage');

  } else {
    window.location.href = './popup.html';
  }
});


function getItemHTML(photo) {
  let itemTemplateSrc = $('#mail-item-template').html();
  return microTemplate(itemTemplateSrc, photo);
}

//filter param template
function microTemplate(src, data) {
  return src.replace(/\{\{([\w\-_\.]+)\}\}/gi, function (match, key) {

    // walk through objects to get value
    var value = data;
    key.split('.').forEach(function (part) {
      value = value[part];
    });
    return value;
  });
}

function updateFlag(flag, mail, token) {
  let rwId = mail.attr('rw_id');
  let url = URL_API + '/pmd-new/Update_flag' + '?token=' + token + '&type=extension' + '&rwInfo={rw_id: ' + rwId + ',flag: ' + flag + '}';

  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    success: function (data, textStatus) {
      mail.remove();
    },
    fail: function (xhr, textStatus, errorThrown) {
      alert('Update flag failed');
    }
  });
}

function exclude(mail, token) {
  updateFlag(true, mail, token)

}

function keep(mail, token) {
  updateFlag(false, mail, token)
}