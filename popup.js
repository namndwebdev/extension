
// 'use strict';
const URL_API = 'http://localhost:8080';
let btnLogin = document.getElementById('btnLogin');

chrome.storage.local.get(['key'], function (result) {
  if (result.key && result.key.loggedIn) {
    window.location.href = './list-mail.html';
  }
});

btnLogin.addEventListener('click', function (element) {
  let userName = document.getElementById('userName').value;
  let userPassword = document.getElementById('userPassword').value
  $.ajax({
    type: 'GET',
    dataType: 'json',
    url: URL_API + '/pmd-new/LoginAPI?userInfo={userName: ' + userName + ', password: ' + userPassword + '}',
    timeout: 5000,
    success: function (data, textStatus) {
      if (data && data.loggedIn) {
        chrome.storage.local.set({ key: data }, function () {
          window.location.href = './list-mail.html';
        });
      } else {
        alert('login failed');
      }
    },
    fail: function (xhr, textStatus, errorThrown) {
      alert('Error');
    }
  });
});

